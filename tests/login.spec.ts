import { test, expect } from '@playwright/test';

test.describe('SauceDemo Login', () => {
  let page;

  test.beforeEach(async ({ browser }) => {
    page = await browser.newPage();
    await login(page, 'standard_user', 'secret_sauce');
  });

  test('Login with standard user', async () => {
    // Verify successful login with standard user
    const title = await page.title();
    expect(title).toContain('Swag Labs');
  });

  test('Login with locked out user', async () => {
    // Try to login with locked out user
    await login(page, 'locked_out_user', 'secret_sauce');
    // Verify error message
    const errorMessage = await page.textContent('.error-message-container');
    expect(errorMessage).toContain('Sorry, this user has been locked out.');
  });

  test('Login with problem user', async () => {
    // Try to login with problem user
    await login(page, 'problem_user', 'secret_sauce');
    // Verify error message
    const errorMessage = await page.textContent('.error-message-container');
    expect(errorMessage).toContain('Sorry, that problem_user is not a valid user.');
  });

  async function login(page, username, password) {
    await page.goto('https://www.saucedemo.com/v1/index.html');
    await page.fill('#user-name', username);
    await page.fill('#password', password);
    await page.click('#login-button');
    // Wait for navigation
    // await page.waitForNavigation({ timeout: 1200000 });
  }
});
