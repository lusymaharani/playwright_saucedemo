import { test, expect } from '@playwright/test';
import { setupLogin } from '../fixtures/login.fixtures';

test.describe('SauceDemo', () => {
  let page;

  // Fixture for login
  test.beforeEach(async ({ browser }) => {
    const loginFixture = await setupLogin(browser);
    page = loginFixture.page;
  });

  test('Login with valid credentials', async () => {
    // Verify successful login
    const title = await page.title();
    expect(title).toContain('Swag Labs');
  });

  test('View product details', async () => {
    // Click on a product and verify details
    await page.click('text=Sauce Labs Bolt T-Shirt');
    const productName = await page.textContent('.inventory_details_name');
    expect(productName).toContain('Sauce Labs Bolt T-Shirt');
  });

  test('Checkout process', async () => {
    // Add a product to cart and proceed to checkout
    await page.click('text=ADD TO CART');
    await page.click('text=REMOVE');
    await page.click('text=ADD TO CART');
    await page.click('text=CHECKOUT');
    // Proceed to checkout
    await page.fill('#first-name', 'John');
    await page.fill('#last-name', 'Doe');
    await page.fill('#postal-code', '12345');
    await page.click('text=CONTINUE');
    // Verify checkout complete
    const title = await page.textContent('.complete-header');
    expect(title).toContain('THANK YOU FOR YOUR ORDER');
  });

  test('Logout', async () => {
    // Verify logout
    await page.click('#react-burger-menu-btn');
    await page.click('#logout_sidebar_link');
    const title = await page.title();
    expect(title).toContain('Swag Labs');
  });
});
