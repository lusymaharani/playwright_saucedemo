import { Page, Browser } from 'playwright';

async function login(page: Page, username: string, password: string) {
  await page.goto('https://www.saucedemo.com/v1/index.html');
  await page.fill('#user-name', username);
  await page.fill('#password', password);
  await page.click('#login-button');
  // Wait for navigation
  await page.waitForNavigation();
}

async function logout(page: Page) {
  await page.click('#react-burger-menu-btn');
  await page.click('#logout_sidebar_link');
}

async function setupLogin(browser: Browser): Promise<{ page: Page, cleanup: () => Promise<void> }> {
  const page = await browser.newPage();
  await login(page, 'standard_user', 'secret_sauce');
  
  const cleanup = async () => {
    await logout(page);
    await page.close();
  };

  return { page, cleanup };
}

export { setupLogin };
